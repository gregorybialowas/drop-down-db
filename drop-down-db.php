<?php
/*  ***     drop down menu function served for DB selection      ***  */
/*  ***     usage: dropMenuDB('deliverto', $row['deliverto'], 'staff', 'id', 'name', '- Select Staff -', 'jobtypeID = '5' && active > '0'');       ***  */
function dropMenuDB($f_name, $f_value, $f_table, $f_key, $f_val, $f_default, $f_whereclause='', $f_css='', $f_tabindex='', $f_js='')
{
    echo '<select name="' . $f_name . '" id="' . $f_name . '"';

    if ($f_css != '') echo ' class="' . $f_css . '"';
    if ($f_tabindex != '') echo ' tabindex="' . $f_tabindex . '"';
    if ($f_js != '') echo ' ' . $f_js;
    if ($f_whereclause != '') $wcl = ' where ' . $f_whereclause; else $wcl = '';

    echo '>';

    echo'<option value="">' . $f_default . '</option>
';

        $sql = mysql_query("select * from " . $f_table . $wcl . "") or die (mysql_error());
        while ($row = mysql_fetch_array($sql))
        {

echo '<option value="' . $row[$f_key] . '"';
            if ($row[$f_key] == $f_value)
            {
            echo ' selected';
            }

echo '>';

        /*  ***  find how many columns were inserted       ***  */
        $colElements = array();
        $counter = 0;

        $colElements = explode (', ', $f_val);
        $countit = count($colElements);

            while ($counter < $countit) {
              echo $row[$colElements[$counter]] . ' ';
              $counter++;
            }

echo '</option>
';
        }
    echo"</select>";
    /*  ***      dropMenuDB end     ***  */
}